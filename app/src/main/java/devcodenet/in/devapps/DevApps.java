package devcodenet.in.devapps;

import android.app.ListActivity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import io.fabric.sdk.android.Fabric;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class DevApps extends ListActivity {
    private PackageManager packageManager = null;
    private List<ApplicationInfo> applist = null;
    private ApplicationAdapter listadaptor = null;
    SwipeRefreshLayout swiperefresh;
    AutoCompleteTextView autotext;
    private ListView list;
    private InterstitialAd mInterstitialAd;
    private StorageReference mStorageRef;
    private ArrayDeque mDatabase;
    ProgressDialog progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_dev_apps);

        mStorageRef = FirebaseStorage.getInstance().getReference();

        MobileAds.initialize(this,"ca-app-pub-6462989004506783~5175368549");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-6462989004506783/6740255711");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                mInterstitialAd.isLoaded();
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
            }
        });

        swiperefresh=(SwipeRefreshLayout)findViewById(R.id.swiperefresh);
        list=(ListView)findViewById(android.R.id.list);

        packageManager = getPackageManager();
        new LoadApplications().execute();

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recreate();
            }
        });


                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView packagename=(TextView)view.findViewById(R.id.app_package);
                TextView appname=(TextView)view.findViewById(R.id.app_name);
                ImageView appicon=(ImageView) view.findViewById(R.id.app_icon);

                PackageManager pm = getPackageManager();
                Intent intent = pm.getLaunchIntentForPackage(packagename.getText().toString());
                startActivity(intent);

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
              ActivityCompat.finishAffinity(this);
        }
        else {
              ActivityCompat.finishAffinity(this);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }



    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
        for (ApplicationInfo info : list) {
            try {
                if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
                    applist.add(info);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return applist;
    }

    private class LoadApplications extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            applist = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));
            listadaptor = new ApplicationAdapter(DevApps.this,R.layout.applistlayout, applist);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setListAdapter(listadaptor);
                }
            });
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            list.invalidate();
            progress.dismiss();
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(DevApps.this, null, "Please Wait...");
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


    public void upload(View view)
    {
        LinearLayout vwParentRow = (LinearLayout)view.getParent();
        TextView packagename=(TextView)vwParentRow.findViewById(R.id.app_package);
        TextView appname=(TextView)vwParentRow.findViewById(R.id.app_name);
        ImageView appicon=(ImageView) vwParentRow.findViewById(R.id.app_icon);
        Uri file= Uri.fromFile(GetApkFile.getApkFile(DevApps.this,packagename.getText().toString()));
        progress.show();
        progress.setMessage("Uploading Please Wait....");

        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageRef = firebaseStorage.getReference();
        final StorageReference uploadeRef = storageRef.child(appname.getText().toString()).child(packagename.getText().toString()+".apk");

        uploadeRef.putFile(file).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>(){
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot){
                progress.dismiss();
                Toast.makeText(DevApps.this, "File has been uploaded to cloud storage", Toast.LENGTH_SHORT).show();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progress.dismiss();
                Toast.makeText(DevApps.this, "Failed has been uploaded to cloud storage", Toast.LENGTH_SHORT).show();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                // progress percentage
                double progressint = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                // percentage in progress dialog
                progress.setMessage("Uploaded " + ((int) progressint) + "%...");
            }
        });

    }

    public  void shareapk(View view)
    {
        LinearLayout vwParentRow = (LinearLayout)view.getParent();
        TextView packagename=(TextView)vwParentRow.findViewById(R.id.app_package);
        TextView appname=(TextView)vwParentRow.findViewById(R.id.app_name);
        ImageView appicon=(ImageView) vwParentRow.findViewById(R.id.app_icon);
        File file=GetApkFile.getApkFile(DevApps.this,packagename.getText().toString());
        Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        if(file.exists()) {
            intentShareFile.setType("application/apk");
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+file));

            intentShareFile.putExtra(Intent.EXTRA_SUBJECT, "Sharing File...");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");
            startActivity(Intent.createChooser(intentShareFile, "Share File"));

        }
    }



}